package com.vinit.firebasechat.chat

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.vinit.firebasechat.BaseActivity
import com.vinit.firebasechat.Constant
import com.vinit.firebasechat.Constant.Companion.EXTRA_USER_NAME
import com.vinit.firebasechat.Constant.Companion.MESSAGES_CHILD
import com.vinit.firebasechat.Constant.Companion.REQUEST_IMAGE
import com.vinit.firebasechat.Constant.Companion.UNSEEN_MSG_COUNT
import com.vinit.firebasechat.Constant.Companion.USERS_CHILD
import com.vinit.firebasechat.R
import com.vinit.firebasechat.model.Message
import com.vinit.firebasechat.util.Utils
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : BaseActivity() {

    val TAG = "ChatActivity"

    private lateinit var userName: String
    private lateinit var chatWith: String

    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mFirebaseDatabaseReference: DatabaseReference
    private lateinit var mFirebaseAdapter: FirebaseRecyclerAdapter<Message, MessageViewHolder>

    private lateinit var userMessagesRef: DatabaseReference
    private lateinit var chatWithMessagesRef: DatabaseReference
    private lateinit var receiverUnSeenCountRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        chatWith = intent.getStringExtra(EXTRA_USER_NAME)
        PreferenceManager.getDefaultSharedPreferences(applicationContext).getString(EXTRA_USER_NAME, null)?.let {
            userName = it
        }

        setupToolbar()

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().reference
        userMessagesRef = mFirebaseDatabaseReference.child(MESSAGES_CHILD).child("${userName}_$chatWith")
        chatWithMessagesRef = mFirebaseDatabaseReference.child(MESSAGES_CHILD).child("${chatWith}_$userName")

        receiverUnSeenCountRef = mFirebaseDatabaseReference.child(USERS_CHILD).child(chatWith).child("${chatWith}_${userName}_$UNSEEN_MSG_COUNT")
        mFirebaseDatabaseReference.child(USERS_CHILD).child(userName).child("${userName}_${chatWith}_$UNSEEN_MSG_COUNT").setValue(0) //reset unseen count value for user

        setupAdapter()

        addMessageImageView.setOnClickListener {
            if (Build.VERSION.SDK_INT < 19) {
                startActivityForResult(Intent.createChooser(Utils.getIntentToSelectImage(), getString(R.string.select_picture)), Constant.REQUEST_IMAGE)
            } else {
                startActivityForResult(Utils.getIntentToSelectImage(), Constant.REQUEST_IMAGE)
            }
        }

        messageEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                sendButton.isEnabled = charSequence.toString().isNotBlank()
            }
            override fun afterTextChanged(editable: Editable) {}
        })

        sendButton.setOnClickListener {
            val message = Message(messageEditText.text.toString(), userName)
            writeMessageToFirebase(message)
        }
    }

    private fun setupAdapter() {
        mLinearLayoutManager = LinearLayoutManager(this)
        mLinearLayoutManager.stackFromEnd = true

        val requestOptions = RequestOptions.placeholderOf(R.drawable.image_outline)

        val options = FirebaseRecyclerOptions.Builder<Message>()
            .setQuery(userMessagesRef, Message::class.java)
            .build()

        mFirebaseAdapter = object : FirebaseRecyclerAdapter<Message, MessageViewHolder>(options) {
            override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MessageViewHolder {
                val inflater = LayoutInflater.from(viewGroup.context)
                return MessageViewHolder(inflater.inflate(R.layout.item_message, viewGroup, false))
            }
            override fun onBindViewHolder(viewHolder: MessageViewHolder, position: Int, message: Message) {
                hideLoading()
                viewHolder.bindToMessage(message, requestOptions)
            }
        }

        mFirebaseAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                val friendlyMessageCount = mFirebaseAdapter.itemCount
                val lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition()
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 || positionStart >= friendlyMessageCount - 1 && lastVisiblePosition == positionStart - 1) {
                    messageRecyclerView.scrollToPosition(positionStart)
                }
            }
        })

        messageRecyclerView.layoutManager = mLinearLayoutManager
        messageRecyclerView.adapter = mFirebaseAdapter
    }

    private fun writeMessageToFirebase(message: Message) {
        showLoading()
        userMessagesRef.push().setValue(message)
        chatWithMessagesRef.push().setValue(message)
        messageEditText.setText("")
        hideLoading()
        incrementUnseenMessageCount()
    }

    private fun incrementUnseenMessageCount() {
        receiverUnSeenCountRef.addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, databaseError.toString())
            }
            override fun onDataChange(snapshot: DataSnapshot) {
                var count = 1L //in the first run, snapshot.value will null so set it to 1 by default
                if (snapshot.value !=null){
                    count = snapshot.value as Long
                    count++
                }
                receiverUnSeenCountRef.setValue(count)
            }
        })
    }

    private fun setupToolbar() {
        val actionBar = supportActionBar
        actionBar?.title = chatWith
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()
        mFirebaseAdapter.stopListening()
        hideLoading()
    }

    override fun onResume() {
        super.onResume()
        mFirebaseAdapter.startListening()
        showLoading()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE && resultCode==Activity.RESULT_OK && data!=null){
            val uri = data.data!!
            val storageReference = FirebaseStorage.getInstance().getReference(userName).child(uri.lastPathSegment!!)
            putImageInStorage(storageReference, uri)
        }
    }

    private fun putImageInStorage(storageReference: StorageReference, uri: Uri) {
        showLoading()
        val uploadTask = storageReference.putFile(uri)
        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let { throw it }
            }
            return@Continuation storageReference.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                writeMessageToFirebase(Message("", userName, downloadUri.toString()))
            } else {
                // Handle failures
                hideLoading()
                Log.w(TAG, getString(R.string.image_upload_failed), task.exception)
            }
        }
    }

    override fun showLoading() {
        super.showLoading()
        sendButton.isEnabled = false
        addMessageImageView.isEnabled = false
    }

    override fun hideLoading() {
        super.hideLoading()
        sendButton.isEnabled = true
        addMessageImageView.isEnabled = true
    }

}
