package com.vinit.firebasechat.chat

import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.storage.FirebaseStorage
import com.vinit.firebasechat.model.Message
import kotlinx.android.synthetic.main.item_message.view.*

class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindToMessage(message: Message, requestOptions: RequestOptions) {

        itemView.tv_sender_name.text = message.senderUserName

        if (message.message.isNotEmpty()){
            itemView.tv_message.text = message.message

            itemView.tv_message.visibility = View.VISIBLE
            itemView.iv_message.visibility = View.GONE

        }else if (message.imageUri.isNotEmpty()){
            val imageUrl = message.imageUri
            if (imageUrl.startsWith("gs://")) {
                //load image
                val storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(imageUrl)
                storageReference.downloadUrl.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUrl = task.result!!.toString()
                        Glide.with(itemView.iv_message.context)
                            .setDefaultRequestOptions(requestOptions)
                            .asBitmap()
                            .load(downloadUrl)
                            .into(itemView.iv_message)
                    } else {
                        Log.w("beta", "Getting download url was not successful.", task.exception)
                    }
                }
            }else{
                Glide.with(itemView.iv_message.context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(message.imageUri)
                    .into(itemView.iv_message)
            }
            itemView.tv_message.visibility = View.GONE
            itemView.iv_message.visibility = View.VISIBLE
        }
    }
}
