package com.vinit.firebasechat.users

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vinit.firebasechat.model.User
import kotlinx.android.synthetic.main.item_user.view.*

class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindToUser(user: User) {
        itemView.tv_user_name.text = user.userName
        itemView.tv_unread_count.text = ""
    }
}
