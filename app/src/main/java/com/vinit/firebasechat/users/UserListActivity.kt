package com.vinit.firebasechat.users

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import com.vinit.firebasechat.BaseActivity
import com.vinit.firebasechat.Constant.Companion.EXTRA_USER_NAME
import com.vinit.firebasechat.Constant.Companion.UNSEEN_MSG_COUNT
import com.vinit.firebasechat.R
import com.vinit.firebasechat.chat.ChatActivity
import com.vinit.firebasechat.login.LoginActivity
import com.vinit.firebasechat.model.User
import kotlinx.android.synthetic.main.activity_user_list.*
import kotlinx.android.synthetic.main.item_user.view.*

class UserListActivity : BaseActivity() {

    private lateinit var database: DatabaseReference
    private lateinit var manager: LinearLayoutManager
    private lateinit var currentUserName: String
    private var adapter: FirebaseRecyclerAdapter<User, UserViewHolder>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        setupToolbar()

        database = FirebaseDatabase.getInstance().reference

        PreferenceManager.getDefaultSharedPreferences(applicationContext).getString(EXTRA_USER_NAME, null)?.let {
            currentUserName = it
        }

        setupAdapter()
    }

    private fun setupAdapter() {
        // Set up Layout Manager, reverse layout
        manager = LinearLayoutManager(this)
        manager.reverseLayout = true
        manager.stackFromEnd = true
        usersList.layoutManager = manager

        val options = FirebaseRecyclerOptions.Builder<User>()
            .setQuery(database.child("users"), User::class.java)
            .build()

        adapter = object : FirebaseRecyclerAdapter<User, UserViewHolder>(options) {
            override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): UserViewHolder {
                val inflater = LayoutInflater.from(viewGroup.context)
                return UserViewHolder(inflater.inflate(R.layout.item_user, viewGroup, false))
            }

            override fun onBindViewHolder(viewHolder: UserViewHolder, position: Int, user: User) {
                database.child("users").child(currentUserName).child("${currentUserName}_${user.userName}_$UNSEEN_MSG_COUNT").addValueEventListener(object: ValueEventListener{
                    override fun onCancelled(databaseError: DatabaseError) {}
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        hideLoading()
                        if (dataSnapshot.value!=null && dataSnapshot.value as Long != 0L){
                            viewHolder.itemView.tv_unread_count.visibility = View.VISIBLE
                            viewHolder.itemView.tv_unread_count.text = (dataSnapshot.value as Long).toString()
                        }else{
                            viewHolder.itemView.tv_unread_count.visibility = View.GONE
                        }
                    }
                })

                // Set click listener for the whole post view
                viewHolder.itemView.setOnClickListener {
                    // Launch PostDetailActivity
                    val intent = Intent(applicationContext, ChatActivity::class.java)
                    intent.putExtra(EXTRA_USER_NAME, user.userName)
                    startActivity(intent)
                }

                // Bind user to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToUser(user)
            }
        }

        usersList.adapter = adapter
    }

    private fun setupToolbar() {
        title = getString(R.string.user_list)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_user, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_logout){
            logoutUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logoutUser() {
        PreferenceManager.getDefaultSharedPreferences(applicationContext).edit().clear().apply()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun onStart() {
        super.onStart()
        showLoading()
        adapter?.startListening()
    }

    override fun onStop() {
        super.onStop()
        hideLoading()
        adapter?.stopListening()
    }
}
