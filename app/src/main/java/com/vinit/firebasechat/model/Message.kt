package com.vinit.firebasechat.model

data class Message(var message: String,
                   var senderUserName: String,
                   var imageUri: String){

    constructor(): this("","","")

    constructor(message: String, senderUserName: String): this(message, senderUserName, "")

}