package com.vinit.firebasechat.model

data class User(val firstName: String, val lastName: String, val userName: String, var profileUri: String){
    constructor(): this("","","","") //use by firebase for parsing
}