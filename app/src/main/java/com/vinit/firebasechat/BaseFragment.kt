package com.vinit.firebasechat

import android.util.Log
import androidx.fragment.app.Fragment
import com.vinit.firebasechat.util.makeGone
import com.vinit.firebasechat.util.makeVisible
import kotlinx.android.synthetic.main.progress_layout.*

open class BaseFragment : Fragment(){

    open fun showLoading(){
        progress.makeVisible()
    }

    open fun hideLoading(){
        Log.d("LoginActivity", "inside base")
        progress.makeGone()
    }

}