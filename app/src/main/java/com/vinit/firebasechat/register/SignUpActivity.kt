package com.vinit.firebasechat.register

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.vinit.firebasechat.BaseActivity
import com.vinit.firebasechat.Constant
import com.vinit.firebasechat.Constant.Companion.REQUIRED
import com.vinit.firebasechat.Constant.Companion.USERS_CHILD
import com.vinit.firebasechat.R
import com.vinit.firebasechat.model.User
import com.vinit.firebasechat.util.Utils
import com.vinit.firebasechat.util.showToast
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : BaseActivity() {

    private val TAG = "SignUpActivity"
    private var selectedImageUri: Uri? = null
    private lateinit var mUser: User
    private lateinit var mUsersReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        setupToolbar()

        mUsersReference = FirebaseDatabase.getInstance().getReference(USERS_CHILD)

        iv_profile.setOnClickListener {
            if (Build.VERSION.SDK_INT < 19) {
                startActivityForResult(Intent.createChooser(Utils.getIntentToSelectImage(), getString(R.string.select_picture)),
                    Constant.REQUEST_IMAGE
                )
            } else {
                startActivityForResult(Utils.getIntentToSelectImage(),
                    Constant.REQUEST_IMAGE
                )
            }
        }

        btn_save.isEnabled = true
        btn_save.setOnClickListener {
            if (validate()){
                btn_save.isEnabled = false //to stop user from submitting multiple registration
                mUser = User(
                    et_first_name.text.toString(),
                    et_last_name.text.toString(),
                    et_user_name.text.toString(),
                    ""
                )
                uploadImageToStorage()
            }
        }
    }

    private fun uploadImageToStorage() {
        showLoading()
        val storageReference = FirebaseStorage.getInstance().getReference(mUser.userName).child(selectedImageUri!!.lastPathSegment!!)
        val uploadTask = storageReference.putFile(selectedImageUri!!)
        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let { throw it }
            }
            return@Continuation storageReference.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                mUser.profileUri = downloadUri.toString()
                addUserToFirebase()
            } else {
                hideLoading()
                applicationContext.showToast(R.string.image_upload_failed)
            }
        }
    }

    private fun addUserToFirebase() {
        mUsersReference.child(mUser.userName).setValue(mUser) //TODO check for duplicate username
        applicationContext.showToast(R.string.register_successful)
        hideLoading()
        finish()
    }

    override fun hideLoading() {
        super.hideLoading()
        btn_save.isEnabled = true
    }

    private fun validate(): Boolean {

        if (TextUtils.isEmpty(et_first_name.text.toString())){
            et_first_name.error = REQUIRED
            return false
        }

        if (TextUtils.isEmpty(et_last_name.text.toString())){
            et_last_name.error = REQUIRED
            return false
        }

        if (TextUtils.isEmpty(et_user_name.text.toString())){
            et_user_name.error = REQUIRED
            return false
        }

        if (selectedImageUri==null){
            applicationContext?.let {
                applicationContext.showToast(R.string.profile_pic_error)
            }
            return false
        }

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.REQUEST_IMAGE && resultCode== Activity.RESULT_OK && data!=null){
            selectedImageUri = data.data!!
            iv_profile.setImageURI(null)
            iv_profile.setImageURI(selectedImageUri)
        }
    }

    private fun setupToolbar() {
        val actionBar = supportActionBar
        actionBar?.title = getString(R.string.register)
    }
}
