package com.vinit.firebasechat.login

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.lifecycle.Observer
import com.vinit.firebasechat.BaseActivity
import com.vinit.firebasechat.Constant
import com.vinit.firebasechat.R
import com.vinit.firebasechat.register.SignUpActivity
import com.vinit.firebasechat.users.UserListActivity
import com.vinit.firebasechat.util.obtainViewModel
import com.vinit.firebasechat.util.replaceFragmentInActivity

class LoginActivity : BaseActivity() {

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupToolbar()

        setupLoginFragment()

        viewModel = obtainViewModel().apply {
            loginSuccessEvent.observe(this@LoginActivity, Observer { userName ->
                saveUsernameInPreference(userName)
                openUserListActivity()
            })

            registerEvent.observe(this@LoginActivity, Observer {
                openSignupActivity()
            })
        }
    }

    private fun saveUsernameInPreference(userName: String) {
        val mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        mSharedPreferences.edit().putString(Constant.EXTRA_USER_NAME, userName).apply()
    }

    private fun setupLoginFragment() {
        supportFragmentManager.findFragmentById(R.id.contentFrame)
            ?: replaceFragmentInActivity(LoginFragment.newInstance(), R.id.contentFrame)
    }

    private fun setupToolbar() {
        title = getString(R.string.login)
    }

    fun openUserListActivity() {
        startActivity(Intent(this, UserListActivity::class.java))
        finish()
    }

    fun openSignupActivity() {
        startActivity(Intent(this, SignUpActivity::class.java))
    }

    fun obtainViewModel(): LoginViewModel = obtainViewModel(LoginViewModel::class.java)

}
