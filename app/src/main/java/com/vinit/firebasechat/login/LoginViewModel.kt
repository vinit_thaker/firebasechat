package com.vinit.firebasechat.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.*
import com.vinit.firebasechat.Constant
import com.vinit.firebasechat.R
import com.vinit.firebasechat.model.User

class LoginViewModel() : ViewModel() {

    private var database: DatabaseReference = FirebaseDatabase.getInstance().reference

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean>
        get() = _dataLoading

    private val _loginSuccessEvent = MutableLiveData<String>()
    val loginSuccessEvent: LiveData<String>
        get() = _loginSuccessEvent

    private val _registerEvent = MutableLiveData<Any>()
    val registerEvent: LiveData<Any>
        get() = _registerEvent

    private val _toastMessage = MutableLiveData<Int>()
    val toastMessage: LiveData<Int>
        get() = _toastMessage

    fun checkUserName(userName: String) {
        _dataLoading.postValue(true)
        val userReference = database.child(Constant.USERS_CHILD).child(userName)
        userReference.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                _dataLoading.postValue(false)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                _dataLoading.postValue(false)
                val user: User? = dataSnapshot.getValue(User::class.java)
                if (user==null){
                    _toastMessage.postValue(R.string.user_not_found)
                }else{
                    _toastMessage.postValue(R.string.login_sucessful)
                    _loginSuccessEvent.postValue(userName)
                }
            }
        })
    }

    fun onRegisterClicked(){
        _registerEvent.postValue("")
    }
}
