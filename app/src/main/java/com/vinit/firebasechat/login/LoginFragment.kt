package com.vinit.firebasechat.login

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.vinit.firebasechat.BaseFragment
import com.vinit.firebasechat.Constant
import com.vinit.firebasechat.R
import com.vinit.firebasechat.util.showToast
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment() {

    private lateinit var viewmodel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewmodel = (activity as LoginActivity).obtainViewModel()

        btn_login.setOnClickListener {
            if (validate()){
                viewmodel.checkUserName(et_user_name.text.toString())
            }
        }

        tv_register.setOnClickListener {
            viewmodel.onRegisterClicked()
        }

        viewmodel.let {
            it.dataLoading.observe(this, Observer { showLoading ->
                if(showLoading) showLoading() else hideLoading()
            })

            it.toastMessage.observe(this, Observer { toastMessageResId ->
                context.showToast(toastMessageResId)
            })
        }
    }

    override fun showLoading() {
        super.showLoading()
        btn_login.isEnabled = false //to disable it temp while we try to login
    }

    override fun hideLoading() {
        super.hideLoading()
        btn_login.isEnabled = true
    }

    private fun validate(): Boolean {
        if (TextUtils.isEmpty(et_user_name.text.toString())){
            et_user_name.error = Constant.REQUIRED
            return false
        }
        return true
    }

    companion object {
        fun newInstance() = LoginFragment()
    }

}
