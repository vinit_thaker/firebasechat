package com.vinit.firebasechat

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.vinit.firebasechat.util.makeGone
import com.vinit.firebasechat.util.makeVisible
import kotlinx.android.synthetic.main.progress_layout.*

open class BaseActivity : AppCompatActivity(){

    open fun showLoading(){
        progress.makeVisible()
    }

    open fun hideLoading(){
        Log.d("LoginActivity", "inside base")
        progress.makeGone()
    }

    override fun setTitle(title: CharSequence) {
        val actionBar = supportActionBar
        actionBar?.title = title
    }

}