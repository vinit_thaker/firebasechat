package com.vinit.firebasechat

class Constant{

    companion object {
        const val EXTRA_USER_NAME = "key_username"

        const val REQUEST_IMAGE = 2
        const val REQUIRED = "Required"

        const val MESSAGES_CHILD = "messages"
        const val USERS_CHILD = "users"
        const val UNSEEN_MSG_COUNT = "unseen_count"
    }

}