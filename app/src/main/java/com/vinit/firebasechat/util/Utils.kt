package com.vinit.firebasechat.util

import android.content.Intent
import android.os.Build

class Utils{

    companion object {

        fun getIntentToSelectImage(): Intent {
            return if (Build.VERSION.SDK_INT < 19) {
                val intent = Intent()
                intent.type = "image/jpeg"
                intent.action = Intent.ACTION_GET_CONTENT
                intent
            } else {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE)
                intent.type = "image/jpeg"
                intent
            }
        }

    }

}