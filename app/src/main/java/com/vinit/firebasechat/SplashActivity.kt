package com.vinit.firebasechat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.vinit.firebasechat.login.LoginActivity
import com.vinit.firebasechat.login.LoginFragment
import com.vinit.firebasechat.users.UserListActivity
import com.vinit.firebasechat.util.replaceFragmentInActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if(PreferenceManager.getDefaultSharedPreferences(applicationContext).getString(Constant.EXTRA_USER_NAME, null) == null){
            startActivity(Intent(this, LoginActivity::class.java))
        }else {
            startActivity(Intent(this, UserListActivity::class.java))
        }

        finish()
    }
}
